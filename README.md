# Conduit

Conduit is a Medium.com clone. It is composed by two main applications:

 - the frontend, written in **TypeScript** with the **Vue3** framework;
 - the backend, written in **PHP** with the **Laravel** framework.

More information on both applications in their respective `README.md` files.


## Thanks

Many thanks to Alexey Mezenin and Dongsen for the original codebases.
