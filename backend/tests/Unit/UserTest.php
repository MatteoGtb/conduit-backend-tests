<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use App\Models\User;

class UserTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
    }


    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->assertTrue(true);
    }

    public function test_getRouteKeyNames(){
        $user = new User();
        $saucisse = $user->getRouteKeyName();
        $this->assertEquals('username', $saucisse);
    }


    public function test_articles() {
        $user = User::all()->firstWhere('username', 'Rose');
        $lardon = $user->articles;
        $this->assertEquals(1, sizeof($lardon));
    }

    // public function test_favouriteArticles(){

    // }

    // public function test_followers(){

    // }

    // public function test_following(){

    // }

    public function test_doesUserFollowAnotherUser(){
        $user = User::all()->firstWhere('username', 'Musonda');
        $this->assertTrue($user->doesUserFollowAnotherUser(2, 3));
    }

    // public function test_doesUserFollowArticle(){

    // }

    // public function test_setPasswordAttribute(){

    // }

    // public function test_getJWTIdentifier(){

    // }
}

