<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory('')->create();

        DB::table('users')->insert([
            'username' => 'Rose',
            'email' => 'rose@mail.com',
            'password' => Hash::make('pwd'),
            'image' => null,
            'bio' => 'Je voudrais devenir enseignante pour enfants',
            'created_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'username' => 'Musonda',
            'email' => 'musonda@mail.com',
            'password' => Hash::make('pwd2'),
            'image' => null,
            'bio' => 'Je songe à devenir infirmière, j’écris mes réflexions',
            'created_at' => Carbon::now(),
        ]);

        DB::table('followers')->insert([
            'follower_id' => DB::table('users')->where('username', 'Musonda')->first()->id,
            'following_id' => DB::table('users')->where('username', 'Rose')->first()->id,
        ]);

        DB::table('followers')->insert([
            'follower_id' => DB::table('users')->where('username', 'Rose')->first()->id,
            'following_id' => DB::table('users')->where('username', 'Musonda')->first()->id,
        ]);

        DB::table('articles')->insert([
            'user_id' => DB::table('users')->where('username', 'Rose')->first()->id,
            'title' => 'Mon premier article',
            'slug' => 'c\'est quoi ? ',
            'description' => 'men fou',
            'body' => 'arrete',
        ]);

        DB::table('articles')->insert([
            'user_id' => DB::table('users')->where('username', 'Musonda')->first()->id,
            'title' => 'Mon premier article',
            'slug' => 'c\'est quoi ? ',
            'description' => 'men fou',
            'body' => 'arrete',
        ]);

        DB::table('articles')->insert([
            'user_id' => DB::table('users')->where('username', 'Musonda')->first()->id,
            'title' => 'Mon deuxième article',
            'slug' => 'c\'est quoi ? ',
            'description' => 'men fou',
            'body' => 'arrete',
        ]);

        DB::table('tags')->insert([
            'name' => 'education'
        ]);

        DB::table('article_tag')->insert([
            'article_id' => DB::table('articles')->where('user_id', DB::table('users')->where('username', 'Rose')->first()->id )->first()->id,
            'tag_id' => DB::table('tags')->where('name', 'education')->first()->id,
        ]);


        DB::table('comments')->insert([
            'user_id' => DB::table('users')->where('username', 'Rose')->first()->id,
            'article_id' => DB::table('articles')->where('user_id', DB::table('users')->where('username', 'Musonda')->first()->id)->first()->id,
            'body' => 'adore sa manière de concevoir l’éducation des enfants',
        ]);
    }
}
